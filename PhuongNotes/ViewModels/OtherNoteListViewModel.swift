//
//  OtherNoteListViewModel.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 19/02/2023.
//

import SwiftUI
import FirebaseDatabase
import FirebaseAuth

class OtherNoteListViewModel: ObservableObject {
    
    private let ref: DatabaseReference = Database.database().reference()
    @Published var listNote: [Note] = []
    
    init() {}
    
    func fetchData(username: String) {
//        guard let userId = Auth.auth().currentUser?.uid else { return }
        
        ref.child(username).child("notes").observe(DataEventType.value) { [weak self] snapshot, error in
            guard let listNoteDict = snapshot.value as? [String: Any] else { return }
            
            let listKey = listNoteDict.keys
            var notes: [Note] = []

            for listId in listKey {
                if let noteDict = listNoteDict[listId] as? [String: Any] {
                    let note = Note(
                        id: noteDict["id"] as? String ?? "",
                        title: noteDict["title"] as? String ?? "",
                        content: noteDict["content"] as? String ?? "",
                        isFavorite: noteDict["isFavorite"] as? Bool ?? false,
                        timestamp: noteDict["timestamp"] as? String ?? ""
                    )
                    notes.append(note)
                }
            }
            
            DispatchQueue.main.async {
                let notess = notes.sorted(by: >)
                self?.listNote = notess
            }
        }
    }
}
