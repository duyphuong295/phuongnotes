//
//  NoteListViewModel.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 17/02/2023.
//

import SwiftUI
import FirebaseDatabase
import FirebaseAuth

class NoteListViewModel: ObservableObject {

    private let ref: DatabaseReference = Database.database().reference()
    @Published var myNotes: [Note] = []

    init() {}
    
    func fetchData() {
        guard let userId = Auth.auth().currentUser?.uid else { return }
        
        ref.child(userId).child("notes").observe(DataEventType.value) { [weak self] snapshot, error in
            guard let listNoteDict = snapshot.value as? [String: Any] else { return }
            
            let listKey = listNoteDict.keys
            var notes: [Note] = []

            for listId in listKey {
                if let noteDict = listNoteDict[listId] as? [String: Any] {
                    let note = Note(
                        id: noteDict["id"] as? String ?? "",
                        title: noteDict["title"] as? String ?? "",
                        content: noteDict["content"] as? String ?? "",
                        isFavorite: noteDict["isFavorite"] as? Bool ?? false,
                        timestamp: noteDict["timestamp"] as? String ?? ""
                    )
                    notes.append(note)
                }
            }
            
            DispatchQueue.main.async {
                let notess = notes.sorted(by: >)
                self?.myNotes = notess
            }
        }
    }
    
    func deleteNote(note: Note) {
        guard let noteId = note.id, !noteId.isEmpty else {
            print("There is no note id")
            return
        }
        let ref = ref.child(noteId)
        
        ref.removeValue { error, _ in
            print(error as Any)
        }
    }
}
