//
//  NoteDetailViewModel.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 22/02/2023.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class NoteDetailViewModel: ObservableObject {
    private let ref: DatabaseReference = Database.database().reference()
    @Published var myNoteForEdit: Note? = nil
    
    init() {}
    
    func updateNote(note: Note) {
        guard let noteId = note.id, !noteId.isEmpty,
              let userId = Auth.auth().currentUser?.uid else {
            addNewNote(note: note)
            return
        }
        
        let ref = ref.child(userId).child("notes").child(noteId)
        ref.setValue(note.toJSON())
    }
    
    private func addNewNote(note: Note) {
        guard let userId = Auth.auth().currentUser?.uid else { return }
        let ramdomId = randomString(length: 20)
        var newNote = note
        newNote.id = ramdomId
        let ref = ref.child(userId).child("notes").child(ramdomId)
        ref.setValue(newNote.toJSON())
    }
}
