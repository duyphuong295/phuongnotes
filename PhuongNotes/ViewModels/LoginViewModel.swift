//
//  LoginViewModel.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 16/02/2023.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class LoginViewModel: ObservableObject {

    private let ref = Database.database().reference()
    
    func updateUserInfo() {
        guard let currentUser = Auth.auth().currentUser else { return }
        let userId = currentUser.uid
        let email = currentUser.email
        
        let ref = ref.child(userId)
        var user = User()
        user.id = userId
        user.name = currentUser.displayName ?? email?.components(separatedBy: "@").first ?? ""
        user.email = email
        ref.setValue(user.toJSON())
    }
}
