//
//  Note.swift
//  Phuong Notes
//
//  Created by Duy Phuong on 16/02/2023.
//

import Foundation

struct Note: Codable, Identifiable, Comparable {
    var id: String?
    var title: String
    var content: String
    var isFavorite: Bool
    var timestamp: String
    
    static func < (lhs: Note, rhs: Note) -> Bool {
        lhs.timestamp < rhs.timestamp
    }
}
