//
//  User.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 16/02/2023.
//

import Foundation

struct User: Codable, Identifiable {
    var id: String?
    var name: String?
    var email: String?
    var password: String?
    var notes: [String: Note] = [:]
}
