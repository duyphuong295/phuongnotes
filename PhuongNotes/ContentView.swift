//
//  ContentView.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 16/02/2023.
//

import SwiftUI
import FirebaseAuth

enum TypeScreen {
    case login
    case tabbar
}

struct ContentView: View {
    @State private var typeScreen: TypeScreen = .login
    
    var body: some View {
        NavigationView {
            if typeScreen == .login {
                LoginView()
            } else if typeScreen == .tabbar {
                TabBarView()
            }
        }
        .onAppear(perform: {
            Auth.auth().addStateDidChangeListener { auth, user in
                if let _ = user {
                    typeScreen = .tabbar
                } else {
                    typeScreen = .login
                }
            }
        })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
