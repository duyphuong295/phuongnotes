//
//  Codable_Ext.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 22/02/2023.
//

import Foundation

extension Encodable {
    private func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
    
    func toJSON() -> [String: Any] {
        do {
            let params = try asDictionary()
            return params
        } catch {
            print(error)
            return [:]
        }
    }
}
