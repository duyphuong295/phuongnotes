//
//  Date_Ext.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 22/02/2023.
//

import Foundation

extension Date {

    func toString(format: String = "yyyy-MM-dd HH:mm:ss", locale: Locale? = nil) -> String {
        // locale list: https://gist.github.com/jacobbubu/1836273
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = (locale != nil) ? locale : Locale(identifier: "vi_VN")
        return dateFormatter.string(from: self)
    }
}
