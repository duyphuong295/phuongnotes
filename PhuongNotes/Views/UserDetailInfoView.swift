//
//  UserInformationView.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 16/02/2023.
//

import SwiftUI
import FirebaseAuth

struct UserDetailInfoView: View {
    @State private var name: String = ""
    @State private var email: String = ""
    private let profileImage: UIImage = UIImage(systemName: "person.crop.circle") ?? UIImage()
    @State private var isPresented = false
    
    var body: some View {
        NavigationView {
            ZStack {
                VStack {
                    if let profileImage = profileImage {
                        Image(uiImage: profileImage)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 120, height: 120)
                            .clipShape(Circle())
                    }
                    Text(name)
                        .font(.largeTitle)
                        .fontWeight(.bold)
                        .padding()
                    Text(email)
                        .font(.headline)
                        .foregroundColor(.gray)
                        .padding(.bottom, 20)
                    Button(action: {
                        // Perform an action, such as editing the user's profile
                        self.isPresented.toggle()
                    }) {
                        Text("Edit Profile")
                            .font(.headline)
                            .foregroundColor(.white)
                            .padding()
                            .frame(maxWidth: .infinity)
                            .background(Color.blue)
                            .cornerRadius(10)
                    }
                    .padding()
                    Button(action: {
                        logout()
                    }) {
                        Text("Log Out")
                            .font(.headline)
                            .foregroundColor(.white)
                            .padding()
                            .frame(maxWidth: .infinity)
                            .background(Color.blue)
                            .cornerRadius(10)
                    }
                    .fullScreenCover(isPresented: $isPresented, content: {
                        EditUserInfoView()
                    })
                    .padding(.horizontal, 15)
                    .padding(.vertical, 10)
                }
            }
            .padding()
            .navigationBarTitle("User Information", displayMode: .inline)
            .onAppear {
                getUserInfo()
            }
        }
    }
    
    private func getUserInfo() {
        guard let user = Auth.auth().currentUser else { return }
        // The user's ID, unique to the Firebase project.
        // Do NOT use this value to authenticate with your backend server,
        // if you have one. Use getTokenWithCompletion:completion: instead.
//        let uid = user.uid
        let email = user.email
//        let photoURL = user.photoURL
        var multiFactorString = "MultiFactor: "
        for info in user.multiFactor.enrolledFactors {
            multiFactorString += info.displayName ?? "[DispayName]"
            multiFactorString += " "
        }
        // ...
        self.name = user.displayName ?? ""
        self.email = email ?? ""
    }
    
    private func logout() {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print("Error signing out: %@", signOutError)
        }
    }
}

struct UserInformationView_Previews: PreviewProvider {
    @State static var typeScreen: TypeScreen = .tabbar
    static var previews: some View {
        UserDetailInfoView()
    }
}
