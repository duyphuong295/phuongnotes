//
//  OtherNoteListView.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 16/02/2023.
//

import SwiftUI

struct OtherNoteListView: View {
    @State var notes: [Note] = [Note(id: "noteid1",
                                     title: "Note 1",
                                     content: "Note 1 detail",
                                     isFavorite: false,
                                     timestamp: "timestamp")]
    @StateObject var viewModel = OtherNoteListViewModel()
    @State private var username = ""
    
    var body: some View {
        NavigationView {
            List(notes, id: \.id) { note in
                NavigationLink(destination: NoteDetailView()) {
                    VStack(alignment: .leading) {
                        Text(note.title)
                            .font(.headline)
                        Text(note.content)
                            .font(.subheadline)
                    }
                }
            }
            .searchable(text: $username, prompt: "Type the username") {
                // 1
//                ForEach(APIManager.searchUserNotes(username), id: \.self) { note in
                    // 2
//                    Text(note.title).searchCompletion(note)
//                }
            }
            .navigationBarTitle("Other Notes")
            .listStyle(.insetGrouped)
        }
    }
}

struct OtherNoteListView_Previews: PreviewProvider {
    static var previews: some View {
        OtherNoteListView()
    }
}
