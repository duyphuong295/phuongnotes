//
//  CreateNoteView.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 16/02/2023.
//

import SwiftUI

struct NoteDetailView: View {
    @State private var title = ""
    @State private var content = ""
    @State private var isFavorite = false
    @Environment(\.presentationMode) private var presentationMode
    @State var note: Note?
    
    @StateObject var viewModel = NoteDetailViewModel()

    var body: some View {
        Form {
            Section(header: Text("Note Info")) {
                TextField("Title", text: $title)
                
                TextEditor(text: $content)
            }
            
            Section(header: Text("Options")) {
                Toggle(isOn: $isFavorite) {
                    Text("Favorite")
                }
            }
        }
        .navigationBarTitle("New Note", displayMode: .inline)
        .navigationBarItems(trailing: Button("Save") {
            var newNote = Note(
                title: title,
                content: content,
                isFavorite: isFavorite,
                timestamp: Date().toString()
            )
            if let existNote = note {
                newNote.id = existNote.id
                newNote.timestamp = existNote.timestamp
            }
            viewModel.updateNote(note: newNote)
            presentationMode.wrappedValue.dismiss()
        }.disabled(title.isEmpty || content.isEmpty)
        )
        .onAppear {
            title = note?.title ?? ""
            content = note?.content ?? ""
            isFavorite = note?.isFavorite ?? false
        }
    }
}

struct CreateNoteView_Previews: PreviewProvider {
    static var previews: some View {
        NoteDetailView()
    }
}
