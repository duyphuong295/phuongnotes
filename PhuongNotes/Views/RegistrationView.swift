//
//  RegistrationView.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 16/02/2023.
//

import SwiftUI
import FirebaseAuth

struct RegistrationView: View {
    @State private var email = ""
    @State private var password = ""
    @State private var passwordConfirmation = ""
    @State private var registrationError: String? = nil
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State private var isLoading: Bool = false
    
    var body: some View {
        ZStack {
            VStack {
                Text("Create Account")
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .padding(.bottom, 50)
                TextField("Email", text: $email)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding(.horizontal, 50)
                    .padding(.bottom, 20)
                    .autocapitalization(.none)
                SecureField("Password", text: $password)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding(.horizontal, 50)
                    .padding(.bottom, 20)
                    .autocapitalization(.none)
                SecureField("Confirm Password", text: $passwordConfirmation)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding(.horizontal, 50)
                    .padding(.bottom, 20)
                    .autocapitalization(.none)
                if let error = registrationError {
                    Text(error)
                        .foregroundColor(.red)
                        .padding(.bottom, 20)
                }
                Button(action: {
                    register()
                }) {
                    Text("Create Account")
                        .font(.headline)
                        .foregroundColor(.white)
                        .padding()
                        .frame(width: 220, height: 60)
                        .background(Color.blue)
                        .cornerRadius(30)
                }
            }
            .padding(.top, 100)
            if isLoading { ProgressView { Text("Loading") }}
        }
    }
    
    func register() {
        guard email.isEmail else {
            registrationError = "Please type your email correctly"
            return
        }
        guard password.count >= 6 else {
            registrationError = "Please type your password with at least 6 characters"
            return
        }
        guard passwordConfirmation.count >= 6 else {
            registrationError = "Please type your confirm password with at least 6 characters"
            return
        }
        guard password == passwordConfirmation else {
            registrationError = "Passwords do not match"
            return
        }
        isLoading = true
        Auth.auth().createUser(withEmail: email, password: password) { result, error in
            isLoading = false
            if let error = error {
                registrationError = error.localizedDescription
            } else {
                // Navigate to home screen or perform any other registration-related action
                presentationMode.wrappedValue.dismiss()
                print("User registered successfully")
            }
        }
    }
}

struct RegistrationView_Previews: PreviewProvider {
    static var previews: some View {
        RegistrationView()
    }
}
