//
//  EditUserInformationView.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 16/02/2023.
//

import SwiftUI
import FirebaseAuth

struct EditUserInfoView: View {
    @State private var name: String = ""
    @State private var email: String = ""
    @State private var profileImage: UIImage? = UIImage(systemName: "person.crop.circle")
    
    @Environment(\.presentationMode) var presentationMode
    @State private var isLoading: Bool = false
    @ObservedObject var viewModel = EditUserInfoViewModel()
    
    var body: some View {
        NavigationView {
            ZStack {
                VStack {
                    if let profileImage = profileImage {
                        Image(uiImage: profileImage)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 120, height: 120)
                            .clipShape(Circle())
                    }
                    
                    Button(action: {
                        // Select a new profile image
                    }) {
                        Text("Select Profile Image")
                            .font(.headline)
                            .foregroundColor(.white)
                            .padding()
                            .frame(maxWidth: .infinity)
                            .background(Color.blue)
                            .cornerRadius(10)
                    }
                    .padding(.horizontal, 20)
                    .padding(.vertical, 30)
                    
                    TextField("Username", text: $name)
                        .textFieldStyle(.roundedBorder)
                        .padding(.horizontal, 20)
                        .font(.body).autocapitalization(.none)
                    Text(email)
                        .font(.callout)
                        .fontWeight(.regular)
                        .padding(.top, 10)
                    Button(action: {
                        // Save the changes to the user's information
                        updateUserInfo()
                    }) {
                        Text("Save Changes")
                            .font(.headline)
                            .foregroundColor(.white)
                            .padding()
                            .frame(maxWidth: .infinity)
                            .background(Color.blue)
                            .cornerRadius(10)
                    }
                    .padding(.horizontal, 20)
                    .padding(.vertical, 10)
                }
                if isLoading { ProgressView { Text("Loading") }
                }
            }
            .navigationBarTitle("Edit User Info", displayMode: .inline)
            .onAppear {
                getUserInfo()
            }
        }
        .padding()
    }
    
    private func getUserInfo() {
        guard let user = Auth.auth().currentUser else { return }
        // The user's ID, unique to the Firebase project.
        // Do NOT use this value to authenticate with your backend server,
        // if you have one. Use getTokenWithCompletion:completion: instead.
//        let uid = user.uid
        let email = user.email
//        let photoURL = user.photoURL
        var multiFactorString = "MultiFactor: "
        for info in user.multiFactor.enrolledFactors {
            multiFactorString += info.displayName ?? "[DispayName]"
            multiFactorString += " "
        }
        // ...
        self.name = user.displayName ?? email?.components(separatedBy: "@").first ?? ""
        self.email = email ?? ""
    }
    
    private func updateUserInfo() {
        let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
        let name = self.name
        changeRequest?.displayName = name
        isLoading = true
        changeRequest?.commitChanges { error in
            isLoading = false
            guard error == nil else { return }
            // Need update username on database
            viewModel.updateUserInfo()
            presentationMode.wrappedValue.dismiss()
        }
    }
}

struct EditUserInformationView_Previews: PreviewProvider {
    static var previews: some View {
        EditUserInfoView()
    }
}
