//
//  TabBarView.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 16/02/2023.
//

import SwiftUI

struct TabBarView: View {
    @State private var selection = 0

    var body: some View {
        TabView(selection: $selection) {
            NoteListView()
                .tabItem {
                    Image(systemName: "note")
                    Text("My Notes")
                }
                .tag(0)
            
            OtherNoteListView()
                .tabItem {
                    Image(systemName: "note.text")
                    Text("Other notes")
                }
                .tag(1)
            
            UserDetailInfoView()
                .font(.system(size: 40, weight: .bold, design: .default))
                .tabItem {
                    Image(systemName: "person.crop.circle")
                    Text("Profile")
                }
                .tag(2)
        }
        .accentColor(.red)
    }
}

struct TabBarView_Previews: PreviewProvider {
    static var previews: some View {
        TabBarView()
    }
}
