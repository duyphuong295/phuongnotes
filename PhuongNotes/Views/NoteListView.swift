//
//  NoteListView.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 16/02/2023.
//

import SwiftUI
import FirebaseDatabase

struct NoteListView: View {
    @StateObject var viewModel = NoteListViewModel()
    
    var body: some View {
        NavigationView {
            List(viewModel.myNotes, id: \.id) { note in
                NavigationLink(destination: NoteDetailView(note: note)) {
                    VStack(alignment: .leading) {
                        Text(note.title)
                            .font(.headline)
                        Text(note.content)
                            .font(.subheadline)
                    }
                }
            }
            .navigationTitle("Notes")
            .listStyle(.insetGrouped)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    NavigationLink(destination: NoteDetailView()) {
                        Image(systemName: "plus.circle")
                            .padding()
                            .fixedSize()
                            .frame(maxWidth: .infinity)
                    }
                }
            }
            .onAppear {
                viewModel.fetchData()
            }
        }
    }
}

struct NoteListView_Previews: PreviewProvider {
    static var previews: some View {
        NoteListView()
    }
}
