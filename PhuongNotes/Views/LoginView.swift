//
//  LoginView.swift
//  Phuong Notes
//
//  Created by Duy Phuong on 16/02/2023.
//

import SwiftUI
import FirebaseAuth

struct LoginView: View {
    @State private var email = ""
    @State private var password = ""
    @State private var loginError: String? = nil
    @ObservedObject var viewModel = LoginViewModel()
    @State private var isLoading: Bool = false

    var body: some View {
        NavigationView {
            ZStack {
                VStack {
                    Text("Login")
                        .font(.largeTitle)
                        .fontWeight(.bold)
                        .padding(.bottom, 50)
                    TextField("Email", text: $email)
                        .textFieldStyle(.roundedBorder)
                        .padding(.horizontal, 50)
                        .padding(.bottom, 20)
                        .autocapitalization(.none)
                    SecureField("Password", text: $password)
                        .textFieldStyle(.roundedBorder)
                        .padding(.horizontal, 50)
                        .padding(.bottom, 20)
                        .autocapitalization(.none)
                    if let error = loginError {
                        Text(error)
                            .foregroundColor(.red)
                            .padding(.bottom, 20)
                    }
                    Button(action: {
                        login()
                    }) {
                        Text("Login")
                            .font(.headline)
                            .foregroundColor(.white)
                            .padding()
                            .frame(width: 220, height: 60)
                            .background(Color.blue)
                            .cornerRadius(30)
                    }
                    
                    NavigationLink {
                        RegistrationView()
                    } label: {
                        Text("Register")
                            .font(.headline)
                            .foregroundColor(.white)
                            .padding()
                            .frame(width: 220, height: 60)
                            .background(Color.blue)
                            .cornerRadius(30)
                    }
                }
                if isLoading { ProgressView { Text("Loading") }}
            }
            .padding(.top, 100)
            .onAppear {
                loginError = nil
            }
        }
    }
    
    private func login() {
        guard email.isEmail else {
            loginError = "Please type your email correctly"
            return
        }
        guard password.count >= 6 else {
            loginError = "Please type your password with at least 6 characters"
            return
        }
        isLoading = true
        Auth.auth().signIn(withEmail: email, password: password) { result, error in
            isLoading = false
            if let error = error {
                loginError = error.localizedDescription
            } else {
                viewModel.updateUserInfo()
                // Navigate to home screen or perform any other login-related action
                print("User logged in successfully")
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
