//
//  String_Ext.swift
//  PhuongNotes
//
//  Created by Duy Phuong on 16/02/2023.
//

import Foundation

extension String {
    
    var isPhone: Bool {
        if self == "" || self.count != 10 || self.first != Character("0") {
            return false
        }
        return true
    }
    
    var isEmail: Bool {
        if self == "" { return false }
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        if emailPred.evaluate(with: self) {
            return isNotVietnamese(text: self)
        }
        return false
    }
    
    private func isNotVietnamese(text: String) -> Bool {
        let vneseRegEx = "^[\'=+-_a-zA-Z0-9!@#$%^&*(),.?\":{}|<>\\s]*$"
        let vnesePred = NSPredicate(format:"SELF MATCHES %@", vneseRegEx)
        return vnesePred.evaluate(with: text)
    }
}

func randomString(length: Int) -> String {
    let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    return String((0..<length).map{ _ in letters.randomElement()! })
}
