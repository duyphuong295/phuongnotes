# phuongnotes
App Note demo SwiftUI and Firebase

Kế hoạch triển khai App Phuong notes

1.  Tóm tắt về cách tiếp cận của bạn
- Xác định yêu cầu của ứng dụng: Phương notes là một ứng dụng di động cho phép người dùng tạo, sửa, xem, và xóa các ghi chú. Người dùng có thể tạo tài khoản bằng email và đăng nhập vào ứng dụng.
Cũng như xem danh sách ghi chú của người dùng khác theo username. Sử dụng Firebase để lưu trữ và quản lý dữ liệu.

- Xác định các tính năng cần thiết: Đăng ký, đăng nhập, tạo ghi chú, sửa ghi chú, xóa ghi chú, xem danh sách ghi chú của người dùng hiện tại, tìm kiếm danh sách ghi chú của người dùng khác, xem thông tin người dùng hiện tại, chỉnh sửa thông tin người dùng.

Phác thảo giao diện người dùng: vẽ flow app trên giấy
Gồm 2 luồng:
- Luồng đăng ký, đăng nhập: màn hình login, màn hình register
- Luồng sử dụng chính, xem, tạo, sửa ghi chú: Màn hình chính là một tabview 3 màn hình: Màn hình danh sách ghi chú của tôi, màn hình danh sách ghi chú của user khác có search bar để nhập tên username tìm kiếm, màn hình thông tin user đang đăng nhập

- Các màn hình phụ: Màn hình edit thông tin username, màn hình tạo note mới, màn hình chi tiết, sửa note hiện tại

Tiến hành thiết kế giao diện cơ bản cho các màn hình đã được phác thảo trên giấy. Các chức năng push, pop các màn hình ở 2 luồng

Đăng ký tài khoản google firebase, tạo app để sử dụng dịch vụ
Add firebase SDK, kết nối app với firebase, gọi api 

Tạo data model
- User model
- Note model
- List notes model

Lên danh sách 10 api chức năng cần cho app:
- searchUserNotes
- getListNotes
- createNote
- deleteNote
- editNote
- getUserInfo
- editUserInfo
- login
- logout
- register

Gắn firebase, gọi api, gắn api vô app

Test thử

Tối ưu UI app

2. Thời gian đã bỏ các khía cạnh của ứng dụng của tôi:

- Tìm hiểu SwiftUI, viết UI cơ bản: 1.5 ngày
- Tìm hiểu firebase, tích hợp Firebase vào app, sử dụng các chức năng của firebase: 1.5 ngày

3.  Vấn đề hoặc hạn chế của giải pháp:
